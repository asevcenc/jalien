/**
 *
 */
package utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogRecord;

import org.json.simple.JSONObject;

import alien.api.taskQueue.SendRemoteLog;
import alien.config.ConfigUtils;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;

/**
 * @author marta
 */
public class JobRemoteLogCollector {
	private static final ExecutorService asyncOperations = new CachedThreadPool(16, 1, TimeUnit.MINUTES);

	private static int port = 8001;

	private static String baseLocation = ConfigUtils.getConfig().gets("utils.JobRemoteLogCollector.basePath", "/extra/joblogs");

	private static String targetServer = ConfigUtils.getConfig().gets("utils.JobRemoteLogCollector.traceServer", "aliendb10.cern.ch");

	private static boolean hasLogsLocally = ConfigUtils.getConfig().getb("utils.JobRemoteLogCollector.tracesAreLocal", false);

	private static DatagramSocket senderSocket;

	private static SocketAddress targetServerAddress;

	private static Monitor monitor = MonitorFactory.getMonitor(JobRemoteLogCollector.class.getCanonicalName());

	static {
		try {
			senderSocket = new DatagramSocket();

			targetServerAddress = new InetSocketAddress(InetAddress.getByName(targetServer), port);
		}
		catch (final Exception e) {
			System.err.println("Exception initializing sockets: " + e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * @author marta
	 */
	public static final class LogSender implements Runnable {
		SendRemoteLog logInstance;

		/**
		 * @param buffer
		 * @param len
		 * @throws IOException
		 */
		public LogSender(final byte[] buffer, final int len) throws IOException {
			try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(buffer, 0, len))) {
				logInstance = (SendRemoteLog) ois.readObject();
			}
			catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		/**
		 * @param log
		 */
		public LogSender(final SendRemoteLog log) {
			this.logInstance = log;
		}

		/**
		 * Send or write to disk
		 */
		public void send() {
			if (hasLogsLocally)
				run();
			else
				sendLogRecord();
		}

		private void sendLogRecord() {
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(baos)) {
				oos.writeObject(logInstance);

				final byte[] data = baos.toByteArray();

				final DatagramPacket packet = new DatagramPacket(data, data.length, targetServerAddress);
				senderSocket.send(packet);
			}
			catch (final IOException ioe) {
				System.err.println("Cannot send message: " + ioe.getMessage());
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			String dir = baseLocation + "/" + logInstance.getSite() + "/" + logInstance.getHost() + "/" + logInstance.getVmUIDAgent();
			String element = "JobWrapper";
			if (logInstance.getVmUID().equals(logInstance.getVmUIDAgent())) {
				element = "JobRunner";
			}
			final File f = new File(dir + "/" + element + "-" + logInstance.getVmUID() + ".log");

			if (!f.exists()) {
				final File fDir = f.getParentFile();

				if (!fDir.exists()) {
					try {
						Files.createDirectories(fDir.toPath(), PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-xr-x")));
					}
					catch (IOException ioe) {
						System.err.println("Cannot create " + fDir.getAbsolutePath() + ": " + ioe.getMessage());
						return;
					}
				}
			}

			long writeTime = logInstance.getLogRecord().getMillis();

			if (writeTime > 16244301090L)
				writeTime /= 1000;

			if (writeTime < 1609455600)
				writeTime = System.currentTimeMillis() / 1000;

			final JSONObject json = new JSONObject();
			final LogRecord log =logInstance.getLogRecord();

			json.put("logTs", Long.valueOf(log.getMillis()));
			json.put("level", log.getLevel().toString());
			json.put("class", log.getSourceClassName());
			json.put("method", log.getSourceMethodName());
			json.put("logMessage", log.getMessage());

			Throwable t = log.getThrown();
			int tCount = 0;

			while (t != null) {
				tCount++;
				json.put("t" + tCount + "-Message", t.getMessage());
				json.put("t" + tCount + "-StackTrace", Arrays.toString(t.getStackTrace()));

				t = t.getCause();
			}

			final String line = json.toJSONString();

			try (PrintWriter pw = new PrintWriter(new FileWriter(f, true))) {
				pw.println(line);
			}
			catch (final IOException ioe) {
				System.err.println("Cannot write to " + f.getAbsolutePath() + " : " + ioe.getMessage());
			}

			f.setReadable(true, false);
		}
	}

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(final String[] args) throws IOException {
		try (DatagramSocket serverSocket = new DatagramSocket(port)) {
			final byte[] buffer = new byte[65535];

			final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

			while (true) {
				serverSocket.receive(packet);
				final int len = packet.getLength();

				if (len < 10)
					continue;

				try {
					asyncOperations.submit(new LogSender(buffer, len));
					monitor.incrementCounter("receivedLogss");
				}
				catch (final IOException ioe) {
					System.err.println("Received invalid packet from " + packet.getSocketAddress() + " : " + ioe.getMessage());
					monitor.incrementCounter("invalidMessages");
				}
			}
		}
	}
}
