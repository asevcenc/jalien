package alien.taskQueue.jobsplit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import alien.taskQueue.JDL;

/**
 * @author Haakon
 * @since 2024-07-01
 */
public class SplitCustom extends JobSplitter {

	@Override
	List<JDL> splitJobs(final JDL j, final long masterId) throws IOException {
		final List<String> subJDLs = j.getInputList(false, "SplitDefinitions");
		if (subJDLs.isEmpty())
			throw new IOException("Can not split by custom, no JDL defined in SplitDefinitions");

		final List<JDL> subjobs = new ArrayList<>();
		for (final String subJDL : subJDLs) {
			final JDL tmpJDL = new JDL(subJDL);
			final JDL baseJDL = prepareSubJobJDL(tmpJDL, masterId);
			final JDL realJDL = changeFilePattern(baseJDL, tmpJDL.getInputData());
			subjobs.add(realJDL);
		}

		return subjobs;
	}

}
