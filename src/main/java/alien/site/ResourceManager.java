package alien.site;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import alien.config.ConfigUtils;
import lia.util.process.ExternalProcesses;

/**
 * @author marta
 */
public class ResourceManager {

	private AtomicLong disk = new AtomicLong();
	private volatile double vMemory = 0;
	private volatile double rMemory = 0;
	private AtomicLong cpuCores = new AtomicLong(1);
	private AtomicLong oversubscribedCores = new AtomicLong();
	private volatile long ttl;

	ResourceManager(Long workdirsize, Double rmem, Double vmem, Long cpu, Long timing, Long oversubscribedcpu) {
		disk.set(workdirsize.longValue());
		vMemory = vmem.doubleValue();
		rMemory = rmem.doubleValue();
		cpuCores.set(cpu.longValue());
		oversubscribedCores.set(oversubscribedcpu.longValue());
		ttl = timing.longValue();
	}

	/**
	 * Logger
	 */
	static final Logger logger = ConfigUtils.getLogger(ResourceManager.class.getCanonicalName());

	long getDisk() {
		return disk.longValue();
	}

	void setDisk(long wORKDIR_SIZE) {
		disk.set(wORKDIR_SIZE);
	}

	double getVMEM() {
		return vMemory;
	}

	void setVMEM(double vMEM) {
		vMemory = vMEM;
	}

	double getRMEM() {
		return rMemory;
	}

	void setRMEM(double rMEM) {
		rMemory = rMEM;
	}

	long getCPU() {
		return cpuCores.longValue();
	}

	void setCPU(long cpu) {
		cpuCores.set(cpu);
	}

	long getTtl() {
		return ttl;
	}

	void setTtl(long ttl) {
		this.ttl = ttl;
	}

	long getOversubscribedCores() {
		return oversubscribedCores.longValue();
	}

	void setOversubscribedCores(long oversubscribedCores) {
		this.oversubscribedCores.set(oversubscribedCores);
	}

	synchronized boolean substract(ResourceManager rm, boolean extraJob) {
		logger.log(Level.INFO, "Original RM : " + this.toString() + " . Substracting " + (rm != null ? rm.toString() : "null"));
		if (this == rm)
			return false;
		if (rm == null || getClass() != rm.getClass())
			return false;
		this.disk.set(recomputeDiskSpace());
		logger.log(Level.INFO, "The recomputed disk space is " + disk + " MB");
		this.disk.addAndGet(-rm.getDisk());
		this.rMemory -= rm.getRMEM();
		this.vMemory -= rm.getVMEM();
		if (extraJob)
			this.oversubscribedCores.addAndGet(-rm.getCPU());
		else
			this.cpuCores.addAndGet(-rm.getCPU());
		return true;
	}

	synchronized void refill(ResourceManager rm, boolean cpuOversubscription, boolean extraJob) {
		logger.log(Level.INFO, "Original RM : " + this.toString() + " . Refilling with " + rm.toString());
		if (cpuOversubscription && extraJob) {
			this.oversubscribedCores.addAndGet(rm.getCPU());
			logger.log(Level.INFO, "Oversubscribed CPU pool getting back " + rm.getCPU() + " cores.  Total available in pool = " + this.oversubscribedCores);
		}
		else {
			this.cpuCores.addAndGet(rm.getCPU());
			logger.log(Level.INFO, "Regular CPU pool getting back " + rm.getCPU() + " cores.  Total available in pool = " + this.cpuCores);
		}
		// Oversubscribed job is moved to the regular pool
		if (cpuOversubscription && !extraJob && !JobRunner.oversubscribedJobs.empty()) {
			JobRunner.moveExtraJobToRegularPool(rm.getCPU());
		}
		this.disk.addAndGet(rm.getDisk());
		this.rMemory += rm.getRMEM();
		this.vMemory += rm.getVMEM();
	}

	void substractCPU(long cpuToSubstract) {
		this.cpuCores.addAndGet(-cpuToSubstract);
	}

	void addOversubscribedCPU(long cpuToAdd) {
		this.oversubscribedCores.addAndGet(cpuToAdd);
	}

	/**
	 * Re-computes the disk space available
	 *
	 * @return the amount of usable disk space left, in MB
	 */
	public static long recomputeDiskSpace() {
		long recomputedDisk = getFreeSpace((String) JobAgent.siteMap.get("workdir"));
		logger.log(Level.INFO, "Recomputing disk space of " + (String) JobAgent.siteMap.get("workdir") + ". Starting with a free space of " + recomputedDisk);
		synchronized (JobAgent.workDirSizeSync) {
			for (Long runningJob : JobAgent.slotWorkdirsMaxSize.keySet()) {
				long maxSize = JobAgent.slotWorkdirsMaxSize.get(runningJob).longValue() * 1024 * 1024;
				String runningJobWorkdir = (String) JobAgent.siteMap.get("workdir") + "/" + JobAgent.defaultOutputDirPrefix + runningJob;

				Path workdirPath = Paths.get(runningJobWorkdir);
				long workdirSize = 0;
				try (Stream<Path> walk = Files.walk(workdirPath)) {
					workdirSize = walk
							.filter(p -> p.toFile().isFile())
							.mapToLong(p -> p.toFile().length())
							.sum();
				}
				catch (Exception e) {
					logger.log(Level.INFO, "Could not compute current size of job workdir " + runningJobWorkdir, e);
				}

				recomputedDisk = recomputedDisk + workdirSize - maxSize;
				logger.log(Level.INFO, "WorkdirSize=" + workdirSize + ", maxSize=" + maxSize + ", recomputedDisk=" + recomputedDisk);
			}
		}
		return recomputedDisk > 0 ? recomputedDisk / 1024 / 1024 : 0;
	}

	/**
	 * @param folder
	 * @return amount of free space (in bytes) in the given folder. Or zero if there was a problem (or no free space).
	 */
	public static long getFreeSpace(final String folder) {
		final File folderFile = new File(Functions.resolvePathWithEnv(folder));

		try {
			if (!folderFile.exists())
				folderFile.mkdirs();
		}
		catch (@SuppressWarnings("unused") Exception e) {
			// ignore
		}

		long space = folderFile.getUsableSpace();
		if (space <= 0) {
			// 32b JRE returns 0 when too much space is available

			try {
				final String output = ExternalProcesses.getCmdOutput(Arrays.asList("df", "-P", "-B", "1", folder), true, 30L, TimeUnit.SECONDS);

				try (BufferedReader br = new BufferedReader(new StringReader(output))) {
					String sLine = br.readLine();

					if (sLine != null) {
						sLine = br.readLine();

						if (sLine != null) {
							final StringTokenizer st = new StringTokenizer(sLine);

							st.nextToken();
							st.nextToken();
							st.nextToken();

							space = Long.parseLong(st.nextToken());
						}
					}
				}
			}
			catch (IOException | InterruptedException ioe) {
				System.out.println("Could not extract the space information from `df`: " + ioe.getMessage());
			}
		}

		return space;
	}

	boolean checkSlotResources(boolean oversubscription) {
		disk.set(recomputeDiskSpace());
		if (disk.longValue() <= 10 * 1024) {
			if (!System.getenv().containsKey("JALIEN_IGNORE_STORAGE")) {
				logger.log(Level.WARNING, "There is not enough space left: " + disk + " MB");
				return false;
			}

			logger.log(Level.INFO, "Ignoring the reported local disk space of " + disk + " MB");
		}

		if (cpuCores.intValue() <= 0 && !oversubscription)
			return false;

		if (oversubscription && oversubscribedCores.longValue() <= 0)
			return false;

		if (rMemory < 2 * 1024) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		String s = "ResourceManager with disk = " + disk + ", RMEM = " + rMemory + ", VMEM = " + vMemory + ", cpuCores = " + cpuCores + ", oversubscribedCpuCores = " + oversubscribedCores + ", TTL = "
				+ ttl;
		return s;
	}
}
