package alien.optimizers.priority;

import java.util.logging.Level;
import java.util.logging.Logger;

import alien.config.ConfigUtils;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import alien.optimizers.DBSyncUtils;
import alien.optimizers.Optimizer;
import alien.taskQueue.TaskQueueUtils;
import lazyj.DBFunctions;

/**
 * @author Jorn-Are Flaten
 * @since 2023-12-08
 */
public class JobAgentUpdater extends Optimizer {
	/**
	 * Logger
	 */
	static final Logger logger = ConfigUtils.getLogger(JobAgentUpdater.class.getCanonicalName());

	/**
	 * Monitoring component
	 */
	static final Monitor monitor = MonitorFactory.getMonitor(JobAgentUpdater.class.getCanonicalName());

	@Override
	public void run() {
		logger.log(Level.INFO, "JobAgentUpdater starting");
		this.setSleepPeriod(60 * 1 * 1000); // 1m
		final int frequency = (int) this.getSleepPeriod();

		while (true) {
			try {
				final boolean updated = DBSyncUtils.updatePeriodic(frequency, JobAgentUpdater.class.getCanonicalName(), this);
				if (updated) {
					updateComputedPriority();
				}
			}
			catch (final Exception e) {
				try {
					logger.log(Level.SEVERE, "Exception executing optimizer", e);
					DBSyncUtils.registerException(JobAgentUpdater.class.getCanonicalName(), e);
				}
				catch (final Exception e2) {
					logger.log(Level.SEVERE, "Cannot register exception in the database", e2);
				}
			}

			try {
				logger.log(Level.INFO, "JobAgentUpdater sleeping for " + this.getSleepPeriod() + " ms");
				sleep(this.getSleepPeriod());
			}
			catch (final InterruptedException e) {
				logger.log(Level.SEVERE, "JobAgentUpdater interrupted", e);
			}
		}
	}

	private static void updateComputedPriority() {
		try (DBFunctions db = TaskQueueUtils.getQueueDB()) {
			if (db == null) {
				logger.log(Level.SEVERE, "JobAgentUpdater could not get a DB connection");
				return;
			}

			db.setQueryTimeout(60);

			final String s = "UPDATE JOBAGENT INNER JOIN PRIORITY USING(userId) SET JOBAGENT.priority = PRIORITY.computedPriority";
			try (Timing t = new Timing(monitor, "JobAgentUpdater")) {
				final StringBuilder registerLog = new StringBuilder();

				logger.log(Level.INFO, "JobAgentUpdater deleting unused entries in the JOBAGENT table");

				if (db.query("DELETE FROM JOBAGENT WHERE counter<1"))
					registerLog.append("Deleted ").append(db.getUpdateCount()).append(" JOBAGENT rows\n");
				else {
					registerLog.append("Delete query failed.");

					final Exception e = db.getLastError();

					if (e != null)
						registerLog.append(" Last error message was: ").append(e.getMessage());

					registerLog.append("\n");
				}

				logger.log(Level.INFO, "JobAgentUpdater starting to update priority in JOBAGENT table");
				db.query(s);
				t.endTiming();
				logger.log(Level.INFO, "JobAgentUpdater finished updating JOBAGENT table, took " + t.getMillis() + " ms");

				registerLog.append("Finished updating JOBAGENT table priority values, in ").append(t.getMillis()).append(" ms\n");
				DBSyncUtils.registerLog(JobAgentUpdater.class.getCanonicalName(), registerLog.toString());
			}
		}
	}

}
