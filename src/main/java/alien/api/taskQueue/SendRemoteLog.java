package alien.api.taskQueue;

import java.util.Arrays;
import java.util.List;
import java.util.logging.LogRecord;

import alien.api.OneWayMessage;
import alien.api.Request;
import utils.JobRemoteLogCollector;

/**
 * Send a remote log to the CS
 *
 * @author marta
 * @since
 */
public class SendRemoteLog extends Request implements OneWayMessage {
	/**
	 *
	 */
	private static final long serialVersionUID = 5022083696413315513L;

	private final String site;
	private final String host;
	private final String vmUIDAgent;
	private final String vmUIDWrapper;
	private final LogRecord logRecord;

	/**
	 * @param sitename
	 * @param hostname
	 * @param runnerId
	 * @param wrapperId
	 * @param lr
	 */
	public SendRemoteLog(final String sitename, final String hostname, final String runnerId, final String wrapperId, final LogRecord lr) {
		site = sitename;
		host = hostname;
		vmUIDAgent = runnerId;
		vmUIDWrapper = wrapperId;
		logRecord = lr;
	}

	@Override
	public List<String> getArguments() {
		return Arrays.asList(site, host, vmUIDAgent, vmUIDWrapper, logRecord.getMessage());
	}

	@Override
	public void run() {
		// System.out.println("Sending REMOTE LOG");
		final JobRemoteLogCollector.LogSender log = new JobRemoteLogCollector.LogSender(this);
		log.send();
	}

	/**
	 * @return the attached log record
	 */
	public LogRecord getLogRecord() {
		return logRecord;
	}

	/**
	 * @return site name
	 */
	public String getSite() {
		return site;
	}

	/**
	 * @return host name
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @return Agent UUID
	 */
	public String getVmUIDAgent() {
		return vmUIDAgent;
	}

	/**
	 * @return self UUID
	 */
	public String getVmUID() {
		return vmUIDWrapper;
	}
}
