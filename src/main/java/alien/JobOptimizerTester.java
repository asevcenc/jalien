package alien;

import java.util.logging.Level;
import java.util.logging.Logger;

import alien.config.ConfigUtils;
import alien.optimizers.Optimizer;

/**
 * @author Haakon
 * @since 2024-07-01
 */
public class JobOptimizerTester {

	static final Logger logger = ConfigUtils.getLogger(JobOptimizerTester.class.getCanonicalName());
	
	/**
	 * Run the job splitter alone in a central instance
	 * 
	 * @param args
	 */
	public static void main(final String[] args)  {
		try {
			final Optimizer optclass = (Optimizer) Class.forName("alien.optimizers.JobOptimizer").getConstructor().newInstance();
			logger.log(Level.INFO, "New JobOptimizer instance!");
			optclass.start();
		}
		catch (final ReflectiveOperationException e) {
			logger.log(Level.SEVERE, "Can't instantiate JobOptimizer! ", e);
		}
	}
}
